angular.module('iac').directive('mc', [function() {
  'use strict';

  // the angular controller function
  function controller($scope, $state) {
        // need to test this step by step
    if($scope.question && $scope.question.type === 'mc') {
      $scope.$parent.answer = [];
    }

    $scope.selected = function(selection) {
      if($scope.$parent.answer === undefined) {
        $scope.$parent.answer = [];
      }

      for(var i = 0; i < $scope.$parent.answer.length; i++) {
        if($scope.$parent.answer[i] === selection) {
          $scope.$parent.answer.splice(i,1);
          return;
        }
      }

      $scope.$parent.answer.push(selection);
    };
  }

  function link($scope, $element) {
  	
  }

  return {
    restrict: 'E',
    templateUrl: 'js/directives/mc.html',
    controller: controller,
    link: link,
  };
}]);