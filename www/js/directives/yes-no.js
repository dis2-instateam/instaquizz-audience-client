angular.module('iac').directive('yesNo', [function() {
  'use strict';

  // the angular controller function
  function controller($scope, $state) {
    
    $scope.yesNoSubmit = function(answer) {
      $scope.$parent.answer = answer;
      $scope.$parent.submit();
    };
  }

  function link($scope, $element) {
  	
  }

  return {
    restrict: 'E',
    templateUrl: 'js/directives/yes-no.html',
    controller: controller,
    link: link,
  };
}]);