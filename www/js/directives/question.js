angular.module('iac').directive('question', [function() {
  'use strict';

  // the angular controller function
  function controller($scope, $state, $http, $interval) {
  	// Address where the server is running
    $scope.address = '46.101.187.127/server';

    var getQuestion = function () {
          // Get the question
      $http.get('http://' + $scope.address + '/api/audience/getQuestion')
      .success(function(data, status, headers, config) {
        $scope.question = data;
      }). 
      error(function(data, status, headers, config) {
        $scope.question = undefined;
      });
    };

    getQuestion();

    $scope.submit = function() {
      // Question expects the type-specific subdirective to set $scope.answer to the correct answer given
      console.log($scope.answer);
      var answer = {
        id: $scope.question.id,
        answer: $scope.answer
      };

      $http.post('http://' + $scope.address + '/api/audience/postAnswer', answer).
        success(function(data, status, headers, config) {
          $scope.lastAnsweredQuestionId = $scope.question.id;
          $scope.question = undefined;
          $scope.answer = undefined;
          getQuestion();
        }
      );
    };

        // Poll the server periodically
    $interval(function() {
      getQuestion();
    },
    1000);


  }

  function link($scope, $element) {
  }

  return {
    restrict: 'E',
    templateUrl: 'js/directives/question.html',
    controller: controller,
    link: link,
  };
}]);