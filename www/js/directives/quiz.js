angular.module('iac').directive('quiz', [function() {
  'use strict';

  // the angular controller function
  function controller($scope, $state) {
    $scope.selectedQuiz = function(selection) {
      console.log('set answer: ' + selection);
      $scope.$parent.answer = selection;
    };
  }

  function link($scope, $element) {
  	
  }

  return {
    restrict: 'E',
    templateUrl: 'js/directives/quiz.html',
    controller: controller,
    link: link,
  };
}]);