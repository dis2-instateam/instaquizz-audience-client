angular.module('iac').directive('typeSpecific', [function() {
  'use strict';

  // the angular controller function
  function controller($scope, $state) {
  }

  function link($scope, $element) {

  }

  return {
    restrict: 'E',
    templateUrl: 'js/directives/type-specific.html',
    controller: controller,
    link: link,
  };
}]);