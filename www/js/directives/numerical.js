angular.module('iac').directive('numerical', [function() {
  'use strict';

  // the angular controller function
  function controller($scope, $state) {
    
    // need to test this step by step
    if($scope.question && $scope.question.type === 'numerical') {
      
      // If the lecturer didnt define a range, use (0,100)
      if(!$scope.question.typeSpecific) {
        $scope.question.typeSpecific = {
          min: 0,
          max: 100
        };
      }

      $scope.$parent.answer = $scope.question.typeSpecific.min;

    }
  }

  function link($scope, $element) {
  	
  }

  return {
    restrict: 'E',
    templateUrl: 'js/directives/numerical.html',
    controller: controller,
    link: link,
  };
}]);