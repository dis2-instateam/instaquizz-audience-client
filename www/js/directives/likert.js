angular.module('iac').directive('likert', [function() {
  'use strict';

  function link($scope, $element) {
    $scope.stars = [false, false, false, false, false];

    $scope.starClicked = function(index) {
      for(var i = 0; i < 5 + 1; i++) {
        $scope.stars[i] = false;
      }
      for(var j = 0; j < index + 1; j++) {
        $scope.stars[j] = true;
      }

      $scope.$parent.answer = index + 1;
    };
  }

  return {
    restrict: 'E',
    templateUrl: 'js/directives/likert.html',
    link: link,
  };
}]);